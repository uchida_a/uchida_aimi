<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
</head>
<body>
   <a href = "./">戻る</a>
   <br>
   <h2>■ ユーザー管理</h2>
   <div class = "header">
       <a href = "signup">新規登録</a>
   </div>

   <br>

   <table border="1" align="left" cellpadding="5">
      <tr>
         <th>管理</th>
         <th>ID</th>
         <th>アカウント名</th>
         <th>名前</th>
         <th>支店</th>
         <th>部署・役職</th>
         <th>ユーザー</th>
      </tr>
      <c:forEach var="users" items="${users}">
         <tr>
            <td>
               <form action="edit" method="get">
                  <input name="id" value="${users.id}" id="id" type="hidden"/>
                  <input type="submit" value="編集" />
               </form>
            </td>

            <td>${users.id}</td>
            <td>${users.account}</td>
            <td>${users.name}</td>
            <td>${users.strBranch}</td>
            <td>${users.strPosition}</td>

            <td>
               <form action="management" method="post">
                  <input name="id" value="${users.id}" type="hidden"/>
                  <input name="user_available" value="true" type="radio"
                     <c:if test="${users.user_available == true}">checked</c:if> />有効
                  <input name="user_available" value="false" type="radio"
                     <c:if test="${users.user_available == false}">checked</c:if> />無効
                  <input type="submit" value="変更" onClick="return confirm('本当に変更しますか？')"/>
               </form>
            </td>

         </tr>
      </c:forEach>
   </table>
</body>
</html>