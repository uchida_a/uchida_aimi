<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <title>ログイン</title>
</head>

<body>
   <form><input type="button" value="戻る" onClick="history.back()"></form>
   <h2>■ ログインページ</h2>

   <div class="main-contents">
      <c:if test="${ not empty errorMessages }">
         <div class="errorMessages">
            <ul>
               <c:forEach items="${errorMessages}" var="message">
                  <li><c:out value="${message}" />
               </c:forEach>
            </ul>
         </div>
         <c:remove var="errorMessages" scope="session"/>
      </c:if>

      <form action="login" method="post">
         <label for="account">ログインID</label>
         <input name="account" id="account"/> <br />

         <label for="password">パスワード</label>
         <input name="password" type="password" id="password"/> <br /><br />

         <input type="submit" value="ログイン" /><br />
      </form>
   </div>
</body>
</html>