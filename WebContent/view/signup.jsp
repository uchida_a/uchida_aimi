<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録画面</title>
</head>
<body>
   <a href = ./management>戻る</a>
   <br>
   <h2>■ ユーザー新規登録</h2>

   <div class="main-contents">
      <c:if test="${ not empty errorMessages }">
         <div class="errorMessages">
            <ul>
               <c:forEach items="${errorMessages}" var="message">
                  <li><c:out value="${message}" />
               </c:forEach>
            </ul>
         </div>
         <c:remove var="errorMessages" scope="session" />
      </c:if>

      <form action="signup" method="post">
         <label for="name">名前</label> <input name="name" id="name" /><br />
         <label for="account">ログインID</label> <input name="account" id="account" /> <br /><br />

         <label for="password">パスワード</label> <input name="password" type="password" id="password" /><br />
         <label for="password2">パスワード(確認用)</label> <input name="password2" type="password" id="password2" /><br /><br />

         <label for="branch">支店</label>
            <select name="branch">
               <option value="1">本社</option>
               <option value="2">支店A</option>
               <option value="3">支店B</option>
               <option value="4">支店C</option>
            </select><br />

         <label for="position">部署・役職</label>
            <select name="position">
               <option value="1">総務人事管理</option>
               <option value="2">情報管理担当</option>
               <option value="3">支店長</option>
               <option value="4">社員</option>
            </select><br /><br />

         <input type="submit" value="登録" />
       </form>
    </div>

</body>
</html>