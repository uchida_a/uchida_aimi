<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${editUser.name}の編集</title>
    </head>
    <body>
        <a href = ./management>戻る</a>
        <h2>■ ${editUser.name}の編集</h2>

        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="edit" method="post">
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>
                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name"/><br />

                <label for="account">ログインID</label>
                <input name="account" value="${editUser.account}" /><br /><br />

                <input name="oldPassword" value="${editUser.password}" id="oldPassword" type="hidden"/>
                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="password2">パスワード(確認用)</label>
                <input name="password2" type="password" id="password2"/> <br /><br />

                <label for="branch">支店</label>
                <select name="branch">
                   <option <c:if test="${editUser.branch == 1}">selected</c:if> value="1">本社</option>
                   <option <c:if test="${editUser.branch == 2}">selected</c:if> value="2">支店A</option>
                   <option <c:if test="${editUser.branch == 3}">selected</c:if> value="3">支店B</option>
                   <option <c:if test="${editUser.branch == 4}">selected</c:if> value="4">支店C</option>
                </select>

                <br />

                <label for="position">部署・役職</label>
                <select name="position">
                   <option <c:if test="${editUser.position == 1}">selected</c:if> value="1">総務人事管理</option>
                   <option <c:if test="${editUser.position == 2}">selected</c:if> value="2">情報管理担当</option>
                   <option <c:if test="${editUser.position == 3}">selected</c:if> value="3">支店長</option>
                   <option <c:if test="${editUser.position == 4}">selected</c:if> value="4">社員</option>
                </select>

                <br /><br />

                <label for="user_available">ユーザーアカウント</label><br />
                <input type="radio" name="user_available"
                   <c:if test="${editUser.user_available == true}">checked</c:if> value="true"/>有効
                <input type="radio" name="user_available"
                   <c:if test="${editUser.user_available == false}">checked</c:if> value="false"/>無効

                <br /><br />

                <input type="submit" value="登録" /> <br />

            </form>
        </div>
    </body>
</html>