<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>
</head>
<body>
   <a href = "./" >戻る</a>
   <br>

   <c:if test="${ not empty errorMessages }">
      <div class="errorMessages">
         <ul>
            <c:forEach items="${errorMessages}" var="message">
               <li><c:out value="${message}" />
            </c:forEach>
         </ul>
      </div>
      <c:remove var="errorMessages" scope="session" />
   </c:if>

   <div class="form-area">
       <c:if test="${ isShowMessageForm }">
          <form action="newpost" method="post">
            <h2>■ 新規投稿</h2>
            <label for="subject">件名</label><br />
			<input name="subject" id="subject"/> <br /><br />
            本文<br />
            <textarea name="message" cols="100" rows="5" class="main-box"></textarea><br /><br />
            <label for="category">カテゴリー</label><br />
			<input name="category" id="category"/> <br /><br />
            <input type="submit" value="投稿する">
          </form>
       </c:if>
    </div>
</body>
</html>