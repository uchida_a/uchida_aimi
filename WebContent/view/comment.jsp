<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コメント画面</title>
</head>
<body>
   <a href = "./">戻る</a>

   <c:if test="${ not empty errorMessages }">
      <div class="errorMessages">
         <ul>
            <c:forEach items="${errorMessages}" var="message">
               <li><c:out value="${message}" />
            </c:forEach>
         </ul>
      </div>
      <c:remove var="errorMessages" scope="session" />
   </c:if>

   <div class="form-area">
   <form action="comment" method="post">
      <h2>■ コメント</h2>
         <input name="message_id" value="${messageId}" type="hidden"/>
         <textarea name="comment" cols="100" rows="5" class="main-box"></textarea><br />
         <input type="submit" value="コメントする">
   </form>
   </div>

   <br><br>

   <div class="comments">
      ============== コメント一覧 ===============
      <c:forEach items="${comments}" var="comment">
         <div class="account-name">
            <span class="account">@<c:out value="${comment.account}" /></span>
            <span class="name"><c:out value="${comment.name}" /></span>
         </div>
         <div class="text">本文：<c:out value="${comment.text}" /></div>
         <div class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
         <form action="deleteComment" method="post">
            <input name="comment_id" value="${comment.id}" type="hidden"/>
            <input name="comment_user_id" value="${comment.userId}" type="hidden"/>
            <input type="submit" value="削除" />
         </form>
      ======================================
      </c:forEach>
   </div>

</body>
</html>