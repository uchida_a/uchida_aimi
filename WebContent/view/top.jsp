<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body>
    <%= new java.util.Date() %>
    <div class="header">

       <c:if test="${ empty loginUser }">
          <a href="login">ログイン</a>
       </c:if>

       <c:if test="${ not empty loginUser }">
          <a href="./">ホーム</a>
          <a href="newpost">新規投稿</a>
          <a href="management">ユーザー管理</a>
          <a href="logout">ログアウト</a>
       </c:if>

       <c:if test="${ not empty errorMessages }">
          <div class="errorMessages">
             <ul>
                <c:forEach items="${errorMessages}" var="message">
                   <li><c:out value="${message}" />
                </c:forEach>
             </ul>
          </div>
          <c:remove var="errorMessages" scope="session" />
       </c:if>

    </div>

    <c:if test="${ not empty loginUser }">
       <div class="profile">
          <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
          <div class="account">@<c:out value="${loginUser.account}" /></div>
       </div>
    </c:if>

    <br><br>

    <div class="narrow">
       【絞り込み検索】<br>
       <form action="narrow" method="post">
          <label for="narrow_category">■ カテゴリー</label> <input name="narrow_category"/><br />
          <label for="narrow_date">■ 日付</label>
             <input type="date"  name="narrow_date1" value="yyyy-mm-dd">
             ～ <input type="date"  name="narrow_date2" value="yyyy-mm-dd"> <br>
          <input type="submit" value="検索" />
       </form>
    </div>

    <br><br>
    ============ 投 稿 一 覧 ============

    <div class="messages">
       <c:forEach items="${messages}" var="message">
          <div class="message">
             <div class="account-name">
                <span class="account">@<c:out value="${message.account}" /></span>
                <span class="name"><c:out value="${message.name}" /></span>
             </div>

             <div class="subject">件名：<c:out value="${message.subject}" /></div>
             <div class="text">本文：<c:out value="${message.text}" /></div>
             <div class="category">カテゴリー：<c:out value="${message.category}" /></div>
             <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
             <form action="comment" method="get">
                 <input name="message_id" value="${message.id}" type="hidden"/>
                 <input type="submit" value="コメント" />
             </form>
             <form action="deleteMessage" method="post">
                <input name="message_id" value="${message.id}" type="hidden"/>
                <input name="message_user_id" value="${message.userId}" type="hidden"/>
                <input type="submit" value="削除" />
             </form>
         </div>
         ===============================<br>
       </c:forEach>
    </div>
</body>
</html>