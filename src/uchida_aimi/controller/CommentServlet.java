package uchida_aimi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uchida_aimi.bean.Comment;
import uchida_aimi.bean.User;
import uchida_aimi.bean.UserComment;
import uchida_aimi.service.CommentService;
import uchida_aimi.utils.CheckUtil;

//コメント
@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int messageId = Integer.parseInt(request.getParameter("message_id"));
		request.setAttribute("messageId", messageId);

		List<UserComment> comments = new CommentService().getComment(Integer.parseInt(request.getParameter("message_id")));
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("view/comment.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

        List<String> errorMessages = new ArrayList<String>();

        if (isValid(request, errorMessages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment();
            comment.setText(request.getParameter("comment"));
            comment.setMessageId(Integer.parseInt(request.getParameter("message_id")));
            comment.setUserId(user.getId());

            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

        String comment = request.getParameter("comment");

        if (StringUtils.isEmpty(comment) == true) {
        	errorMessages.add("コメントを入力してください");
        }
        if(CheckUtil.stringLengthCheck(comment, 2, 1000) < 0){
        	errorMessages.add("コメントは500文字以下で入力してください");
        }
        if (errorMessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
