package uchida_aimi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import uchida_aimi.bean.UserMessage;
import uchida_aimi.service.MessageService;

//絞込検索
@WebServlet("/narrow")
public class NarrowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();

    	String category = request.getParameter("narrow_category");
    	String date1 = request.getParameter("narrow_date1");
    	String date2 = request.getParameter("narrow_date2");

    	if (isValid(request, messages) == true) {
    		List<UserMessage> narrowMessages = new MessageService().getNarrowMessages(category, date1, date2);
        	request.setAttribute("messages", narrowMessages);

        	request.getRequestDispatcher("view/top.jsp").forward(request, response);
    	} else {
    		request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("./").forward(request, response);
    	}
	}

    private boolean isValid(HttpServletRequest request, List<String> messages) {

		String category = request.getParameter("narrow_category");
		String date1 = request.getParameter("narrow_date1");
    	String date2 = request.getParameter("narrow_date2");

		if (StringUtils.isEmpty(category) == true) {
			messages.add("絞り込み条件（カテゴリー）を入力してください");
		}
		if (StringUtils.isEmpty(date1) == true || StringUtils.isEmpty(date2) == true) {
			messages.add("絞り込み条件（期日）を指定してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
