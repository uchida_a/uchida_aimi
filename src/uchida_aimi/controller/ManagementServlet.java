package uchida_aimi.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uchida_aimi.bean.User;
import uchida_aimi.service.UserService;

//ユーザー管理
@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User session = (User) request.getSession().getAttribute("loginUser");
        int branch = session.getBranch();
        int position = session.getPosition();

        if (branch == 1 && position == 1) {
            List<User> users = new UserService().getUsers();
            request.setAttribute("users", users);

    		request.getRequestDispatcher("view/management.jsp").forward(request, response);
        } else {
        	String messages = "ユーザー管理画面を閲覧する権限がありません。";
        	request.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("./").forward(request, response);
        }
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User userAvailable = getUserAvailable(request);
		new UserService().flagUpdate(userAvailable);

        List<User> newUsers = new UserService().getUsers();
        request.setAttribute("users", newUsers);

		request.getRequestDispatcher("view/management.jsp").forward(request, response);
	}

	private User getUserAvailable(HttpServletRequest request) throws IOException, ServletException{

		User userAvailable = new User();
		userAvailable.setUser_available(Boolean.valueOf(request.getParameter("user_available")));
		userAvailable.setId(Integer.parseInt(request.getParameter("id")));

		return userAvailable;
	}
}