package uchida_aimi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uchida_aimi.bean.User;
import uchida_aimi.service.MessageService;

//投稿削除
@WebServlet("/deleteMessage")
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	User session = (User) request.getSession().getAttribute("loginUser");

    	int userId = session.getId();
    	int messageUserId = Integer.parseInt(request.getParameter("message_user_id"));
    	int messageId = Integer.parseInt(request.getParameter("message_id"));

    	if(userId == messageUserId){
    		boolean result = new MessageService().deleteMessage(messageId);
    		response.sendRedirect("./");

    		if(result == false){
    			String messages = "投稿を削除できませんでした";
            	request.setAttribute("errorMessages", messages);
            	request.getRequestDispatcher("./").forward(request, response);
        	}

    	} else {
    		String messages = "権限がないため投稿を削除できません";
        	request.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("./").forward(request, response);
    	}

	}

}
