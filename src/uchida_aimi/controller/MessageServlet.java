package uchida_aimi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uchida_aimi.bean.Message;
import uchida_aimi.bean.User;
import uchida_aimi.service.MessageService;
import uchida_aimi.utils.CheckUtil;

//投稿
@WebServlet(urlPatterns = { "/newpost" })
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("view/newpost.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setSubject(request.getParameter("subject"));
            message.setText(request.getParameter("message"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./newpost");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String subject = request.getParameter("subject");
        String message = request.getParameter("message");
        String category = request.getParameter("category");

        if(StringUtils.isEmpty(subject) == true) {
            messages.add("件名を入力してください");
        } else if(CheckUtil.stringLengthCheck(subject, 2, 60) < 0) {
        	messages.add("件名は30文字以下で入力してください");
        }

        if(StringUtils.isEmpty(message) == true) {
            messages.add("本文を入力してください");
        } else if(CheckUtil.stringLengthCheck(message, 2, 2000) < 0) {
        	messages.add("本文は1000文字以下で入力してください");
        }

        if(StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリーを入力してください");
        } else if(CheckUtil.stringLengthCheck(category, 2, 20) < 0) {
        	messages.add("カテゴリーは10文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
