package uchida_aimi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uchida_aimi.bean.User;
import uchida_aimi.service.CommentService;

//コメント削除
@WebServlet("/deleteComment")
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	User session = (User) request.getSession().getAttribute("loginUser");

    	int userId = session.getId();
    	int commentUserId = Integer.parseInt(request.getParameter("comment_user_id"));
    	int commentId = Integer.parseInt(request.getParameter("comment_id"));

    	if(userId == commentUserId){
    		boolean result = new CommentService().deleteComment(commentId);
    		response.sendRedirect("./");

    		if(result == false){
    			String messages = "コメントを削除できませんでした";
            	request.setAttribute("errorMessages", messages);
            	request.getRequestDispatcher("./").forward(request, response);
        	}

    	} else {
    		String messages = "権限がないためコメントを削除できません";
        	request.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("./").forward(request, response);
    	}
	}
}
