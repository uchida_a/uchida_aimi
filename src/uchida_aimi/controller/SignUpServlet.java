package uchida_aimi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uchida_aimi.bean.User;
import uchida_aimi.service.UserService;
import uchida_aimi.utils.CheckUtil;

//ユーザー情報新規登録
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User session = (User) request.getSession().getAttribute("loginUser");
        int branch = session.getBranch();
        int position = session.getPosition();

        if (branch == 1 && position == 1) {
        	request.getRequestDispatcher("view/signup.jsp").forward(request, response);

        } else {
        	String messages = "ユーザー登録の権限がありません。";
        	request.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("./").forward(request, response);
        }
	}

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setPosition(Integer.parseInt(request.getParameter("position")));

            new UserService().register(user);

            response.sendRedirect("./management");

        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");

        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        } else if(CheckUtil.stringLengthCheck(account, 6, 20) < 0) {
        	messages.add("ログインIDは6文字以上20文字以下で入力してください");
        } else if(CheckUtil.digitAlphabetCheck(account) == false) {
        	messages.add("ログインIDは半角英数字[azAZ0*9]で入力してください");
        }

        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else if(CheckUtil.stringLengthCheck(password, 6, 20) < 0) {
        	messages.add("パスワードは6文字以上20文字以下で入力してください");
        } else if(CheckUtil.digitAlphabetCheck(password) == false) {
        	messages.add("パスワードは半角英数字[azAZ0*9]で入力してください");
        }

        if (!(password.equals(password2))) {
			messages.add("確認用パスワードが一致しません");
		}

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}
}
