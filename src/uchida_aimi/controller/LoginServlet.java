package uchida_aimi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uchida_aimi.bean.User;
import uchida_aimi.service.LoginService;

//ログイン
@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("view/login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String account = request.getParameter("account");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();

        User user = loginService.login(account, password);
        User flag = loginService.flag(account, password);

        HttpSession session = request.getSession();

        if (user != null && flag != null) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else {
            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました。");
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("login");
        }
    }
}
