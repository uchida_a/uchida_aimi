package uchida_aimi.utils;

//文字列チェック
public class CheckUtil {

	/**
	 * 文字列中の文字数を半角文字基準でチェックする
	 *
	 * @param input チェック対象の文字列
	 * @param min 許容最小文字数
	 * @param max 許容最大文字数
	 * @return チェック結果。0なら許容内。負の値なら不足分。正の値なら超過分。
	 */
	static public int stringLengthCheck(String input, int min, int max) {

	    int length = input.getBytes().length;
	    if(length < min) { // 最小文字数よりも少なかった場合
	        return length - min;
	    }
	    if(length > max) { // 最大文字数よりも多かった場合
	        return length - max;
	    }
	    return 0; // 許容内であった場合
	}

	/**
	 * 文字列が半角英数のみで構成されているかをチェックする
	 *
	 * @param input チェック対象の文字列
	 * @return チェック結果。半角英数のみなら true そうでなければ false
	 */
	static public boolean digitAlphabetCheck(String input) {
	    for(int i = 0; i < input.length(); i++) {
	        char c = input.charAt(i);
	        if( (c < '0' || c > '9') && // 数字でない
	            (c < 'a' || c > 'z') && // 小文字アルファベットでない
	            (c < 'A' || c > 'Z') // 大文字アルファベットでない
	        ) {
	        return false;
	        }
	    }
	    return true;
	}
}
