package uchida_aimi.dao;

import static uchida_aimi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import uchida_aimi.bean.UserMessage;
import uchida_aimi.exception.SQLRuntimeException;

//投稿済情報の操作
public class UserMessageDao {

	//名称置換、抽出、ソート
    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);

            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //beanに格納
    private List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setAccount(account);
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;

        } finally {
            close(rs);
        }
    }

    //投稿削除
	public boolean deleteMessage(Connection connection, int messageId){

    	PreparedStatement ps = null;
    	try{
    		String sql = "DELETE FROM messages WHERE id = ?";

    		 ps = connection.prepareStatement(sql);
             ps.setInt(1, messageId);

             ps.executeUpdate();
             return true;
         } catch (SQLException e) {
        	 return false;
         } finally {
             close(ps);
         }
    }

    //投稿取得
    public List<UserMessage> getUserMessages(Connection connection, String category, String date1, String date2) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();

        	sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE category LIKE ? ");
        	sql.append("AND messages.created_date BETWEEN ? AND ?");

        	ps = connection.prepareStatement(sql.toString());
            ps.setString(1, "%" + category + "%");
            ps.setString(2, date1);
            ps.setString(3, date2);

            ResultSet rs = ps.executeQuery(); //sql文実行
            List<UserMessage> ret = toUserMessageList(rs);

            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}