package uchida_aimi.dao;

import static uchida_aimi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import uchida_aimi.bean.User;
import uchida_aimi.exception.NoRowsUpdatedRuntimeException;
import uchida_aimi.exception.SQLRuntimeException;

public class UserDao {

	//ユーザー情報登録
    public void insert(Connection connection, User user) {

    	PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();

            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch");
            sql.append(", position");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", user_available");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch
            sql.append(", ?"); // position
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", true"); // user_available
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getPosition());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ユーザー情報取得
    public User getUser(Connection connection, String account, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //beanにセット
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                String password = rs.getString("password");
                Integer branch = rs.getInt("branch");
                Integer position = rs.getInt("position");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");
                Boolean user_available = rs.getBoolean("user_available");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setName(name);
                user.setPassword(password);
                user.setBranch(branch);
                user.setPosition(position);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);
                user.setUser_available(user_available);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

    //フラグ取得
    public User getFlag(Connection connection, String account, String password){

    	 PreparedStatement ps = null;
    	 try {
             String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

             ps = connection.prepareStatement(sql);
             ps.setString(1, account);
             ps.setString(2, password);

             ResultSet rs = ps.executeQuery();
             List<User> FlagList = toFlagList(rs);

             if(FlagList.isEmpty() == true){
            	 return null;
             } else {
            	 return FlagList.get(0);
             }

    	 } catch (SQLException e) {
    	        throw new SQLRuntimeException(e);
    	 } finally {
    		 close(ps);
    	 }
    }

    private List<User> toFlagList(ResultSet rs) throws SQLException {

    	List<User> ret = new ArrayList<User>();

    	try{
    		while(rs.next()){
              	 Boolean user_available = rs.getBoolean("user_available");

              	 User user = new User();
              	 user.setUser_available(user_available);

              	 if(user.getUser_available() == false){
              		ret.add(null);
              	 } else {
              		ret.add(user);
              	 }
          	 }
        } finally {
            close(rs);
        }
    	return ret;
    }

    //IDからユーザー情報取得
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //usersテーブルまるごと取得
    public List<User> getUsers(Connection connection, int num){

    	PreparedStatement ps = null;
        try {

           StringBuilder sql = new StringBuilder();
           sql.append("SELECT ");
           sql.append("users.id as id, ");
           sql.append("users.account as account, ");
           sql.append("users.password as password, ");
           sql.append("users.name as name, ");
           sql.append("branches.name as branch, ");
           sql.append("positions.name as position, ");
           sql.append("users.created_date as created_date, ");
           sql.append("users.updated_date as updated_date, ");
           sql.append("users.user_available as user_available ");
           sql.append("FROM users ");
           sql.append("INNER JOIN branches ");
           sql.append("ON users.branch = branches.id ");
           sql.append("INNER JOIN positions ");
           sql.append("ON users.position = positions.id ");
           sql.append("ORDER BY id ASC");

           ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList2(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

  //beanにセット
    private List<User> toUserList2(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                String password = rs.getString("password");
                String branch = rs.getString("branch");
                String position = rs.getString("position");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");
                Boolean user_available = rs.getBoolean("user_available");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setName(name);
                user.setPassword(password);
                user.setStrBranch(branch);
                user.setStrPosition(position);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);
                user.setUser_available(user_available);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //ユーザー情報書き換え
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  account = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", branch = ?");
            sql.append(", position = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(", user_available = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getPosition());
            ps.setBoolean(6, user.getUser_available());
            ps.setInt(7, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //フラグ更新
    public void flagUpdate(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
        	String sql = "UPDATE users SET user_available = ? WHERE id = ?";

        	ps = connection.prepareStatement(sql);
            ps.setBoolean(1, user.getUser_available());
            ps.setInt(2, user.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}