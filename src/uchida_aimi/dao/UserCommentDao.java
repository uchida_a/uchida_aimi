package uchida_aimi.dao;

import static uchida_aimi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import uchida_aimi.bean.UserComment;
import uchida_aimi.exception.SQLRuntimeException;

//コメント取得
public class UserCommentDao {

	//commentsテーブルとusers・messagesを内部結合
    public List<UserComment> getComments(Connection connection, int message_id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date, ");
            sql.append("comments.message_id as message_id ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("INNER JOIN messages ");
            sql.append("ON comments.message_id = messages.id ");
            sql.append("WHERE message_id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, message_id);

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);

            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs) throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int messageId = rs.getInt("message_id");

                UserComment comment = new UserComment();
                comment.setAccount(account);
                comment.setName(name);
                comment.setId(id);
                comment.setUserId(userId);
                comment.setText(text);
                comment.setCreated_date(createdDate);
                comment.setMessageId(messageId);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

	public boolean deleteComment(Connection connection, int commentId){

    	PreparedStatement ps = null;
    	try{
    		String sql = "DELETE FROM comments WHERE id = ?";

    		 ps = connection.prepareStatement(sql);
             ps.setInt(1, commentId);

             ps.executeUpdate();
             return true;
         } catch (SQLException e) {
        	 return false;
         } finally {
             close(ps);
         }
    }
}