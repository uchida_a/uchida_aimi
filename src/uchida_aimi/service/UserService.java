package uchida_aimi.service;

import static uchida_aimi.utils.CloseableUtil.*;
import static uchida_aimi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import uchida_aimi.bean.User;
import uchida_aimi.dao.UserDao;
import uchida_aimi.utils.CipherUtil;

//ユーザー情報操作
public class UserService {

	//新規登録
	public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//ユーザー情報取得
	public User getUser(int userId) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User user = userDao.getUser(connection, userId);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	//ユーザー編集
	public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            int n = (user.getPassword()).length();

            if(n > 20){
            	user.setPassword(user.getPassword());
            } else {
            	String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//フラグ更新
	public void flagUpdate(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.flagUpdate(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//usersテーブル一覧表示
	private static final int LIMIT_NUM = 1000;

    public List<User> getUsers() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> ret = userDao.getUsers(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
