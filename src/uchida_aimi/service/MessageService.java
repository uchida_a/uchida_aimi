package uchida_aimi.service;

import static uchida_aimi.utils.CloseableUtil.*;
import static uchida_aimi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import uchida_aimi.bean.Message;
import uchida_aimi.bean.UserMessage;
import uchida_aimi.dao.MessageDao;
import uchida_aimi.dao.UserMessageDao;

//投稿操作
public class MessageService {

	//新規投稿の登録
    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    //投稿を取得
    public List<UserMessage> getMessage() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao userMessageDao = new UserMessageDao();
            List<UserMessage> ret = userMessageDao.getUserMessages(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //投稿削除
    public boolean deleteMessage(int messageId) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserMessageDao userMessageDao = new UserMessageDao();
	        boolean result = userMessageDao.deleteMessage(connection, messageId);

	        commit(connection);

	        return result;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

    //絞込検索
    public List<UserMessage> getNarrowMessages(String category, String date1, String date2) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao userMessageDao = new UserMessageDao();
            List<UserMessage> ret = userMessageDao.getUserMessages(connection, category, date1, date2);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}