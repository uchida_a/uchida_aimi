package uchida_aimi.service;

import static uchida_aimi.utils.CloseableUtil.*;
import static uchida_aimi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import uchida_aimi.bean.Comment;
import uchida_aimi.bean.UserComment;
import uchida_aimi.dao.CommentDao;
import uchida_aimi.dao.UserCommentDao;

//コメント
public class CommentService {

	//新規登録
    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //取得
    public List<UserComment> getComment(int messageId) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserCommentDao userCommentDao = new UserCommentDao();
	        List<UserComment> userComment = userCommentDao.getComments(connection, messageId);

	        commit(connection);

	        return userComment;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

    //削除
    public boolean deleteComment(int commentId) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserCommentDao userCommentDao = new UserCommentDao();
	        boolean result = userCommentDao.deleteComment(connection, commentId);

	        commit(connection);

	        return result;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}