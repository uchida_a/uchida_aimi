package uchida_aimi.service;

import static uchida_aimi.utils.CloseableUtil.*;
import static uchida_aimi.utils.DBUtil.*;

import java.sql.Connection;

import uchida_aimi.bean.User;
import uchida_aimi.dao.UserDao;
import uchida_aimi.utils.CipherUtil;

//ログイン
public class LoginService {

	//ID、PWの照合
    public User login(String account, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);

			User user = userDao.getUser(connection, account, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //フラグの確認
    public User flag(String account, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);

			User flag = userDao.getFlag(connection, account, encPassword);

            commit(connection);

            return flag;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}